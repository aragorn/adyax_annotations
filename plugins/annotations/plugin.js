(function($) {
  CKEDITOR.plugins.add('annotations', {
    init: function(editor) {
      var addCssObj = CKEDITOR;

      if (Drupal.ckeditor_ver == 3) {
        addCssObj = editor;
      }

      addCssObj.addCss(
        'a.annotations' +
        '{' +
          'text-decoration: none;' +
        '}'
      );

      CKEDITOR.dialog.add('annotationsDialog', function(editor) {
        var loadElements = function(editor, selection, element) {
          var content = null;

          this.setValueOf('info', 'annotations', "");
        };
        return {
          title : Drupal.t('Annotations Dialog'),
          minWidth : 500,
          minHeight : 50,
          contents : [
            {
              id : 'info',
              label : Drupal.t('Add an annotation'),
              title : Drupal.t('Add an annotation'),
              elements :
                [
                  {
                    id : 'annotations',
                    type : 'text',
                    label : Drupal.t('Annotation text:')
                  }
                ]
            }
          ],
          onOk : function() {
            var editor = this.getParentEditor();
            var content = this.getValueOf('info', 'annotations');
            if (content.length > 0) {
              var newElement = CKEDITOR.dom.element.createFromHtml('<sup></sup>');
              newElement.setHtml('<a href="#" class="annotations" title="' + content + '">[?]</a> ');
              editor.insertElement(newElement);
            }
          }
        };
      });
      editor.addCommand('annotations', new CKEDITOR.dialogCommand('annotationsDialog'));

      editor.ui.addButton('annotations', {
        label : Drupal.t('Add an annotations'),
        icon : this.path + 'icons/annotations.png',
        command : 'annotations'
      });
    }
  })
})(jQuery);
