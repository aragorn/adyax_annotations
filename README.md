@TODO list:
1. Provide an ability to edit annotation value in CKEditor.
2. Display annotations numbers in CKEditor.
3. Create theme functions for annotations list and annotation links.
4. Add a default styling for annotations elements.